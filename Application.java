import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//Student st = new Student();
		
		//Student brad = new Student();
			//testing setName()
		//System.out.println(brad.getName());
		//brad.setName("Brad");
			//System.out.println(brad.getName());
		//brad.setGrade('C');
		//testing setStudentNumber()
			//System.out.println(brad.getStudentNumber());
		//brad.setStudentNumber(433567);
			//System.out.println(brad.getStudentNumber());
		//print name, grade and student number after making values private
		
		
		//Student emily = new Student();
		//emily.setName("Emily");
		//emily.setGrade('A');
		//emily.setStudentNumber(666666);
		//print name, grade and student number after making values private
		/*System.out.println(emily.getName());
		System.out.println(emily.getGrade());
		System.out.println(emily.getStudentNumber());*/
		
		//Student[] section3 = new Student[3];
		//section3[0] = brad;
		//section3[1] = emily;
		
		//section3[2] = new Student();
		
		//section3[2].setName("Secret");
		//section3[2].setGrade('S');
		//section3[2].setStudentNumber(111110);
		//print name, grade and student number after making values private
		/*System.out.println(section3[2].getName());
		System.out.println(section3[2].getGrade());
		System.out.println(section3[2].getStudentNumber());*/
		
		
		
		//New Student
		Student s = new Student("Eric", 'S');
		s.setStudentNumber(1010);
		System.out.println(s.getName());
		System.out.println(s.getGrade());
		System.out.println(s.getStudentNumber());
		System.out.println(s.getAmountLearnt());
		
		
		
		//Use the get methods on first student
		/*System.out.println(brad.getName());
		System.out.println(brad.getGrade());
		System.out.println(brad.getStudentNumber());
		System.out.println(brad.getAmountLearnt());*/
		
		//Test to see if the if block inside our learn() is working
		/*section3[0].learn(10);
		System.out.println(section3[0].amountLearnt);
		section3[0].learn(-100);
		System.out.println(section3[0].amountLearnt);*/
		
		//Amount studied by the last student
		/*int amountStudied = sc.nextInt();
		for(int i = 0; i < 2; i++) {
			section3[2].learn(amountStudied);
		}*/
		
		//print the amountLearnt attribute by all students
		/*for(int i = 0; section3.length > i; i++) {
			System.out.println(section3[i].amountLearnt);
		}*/
		
		//print attributes of the section3 array when the index = 2
		/*System.out.println(section3[2].getName());
		System.out.println(section3[2].grade);
		System.out.println(section3[2].getSNumber);*/
		
		/*System.out.println(brad.getName());
		System.out.println(brad.grade);
		System.out.println(brad.getSNumber());
		
		System.out.println(emily.getName());
		System.out.println(emily.grade);
		System.out.println(emily.getSNumber());*/
		
		//This one is the incorrect ways of calling the non-static(instance) method
		//Student.welcomeToStudent(brad.getname());
		
		//This is the correct way to do it
		/*st.welcomeToStudent(brad.getName());
		st.welcomeToStudent(emily.getName());*/
	}
}