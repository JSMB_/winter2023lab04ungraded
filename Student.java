public class Student {
	private String name;
	private char grade;
	private int sNumber;
	private int amountLearnt;
	
	//CONSTRUCTORS
	
	public Student(String name, char grade) {
		this.amountLearnt = 0;
		this.name = name;
		this.sNumber = 0000;
		this.grade = grade;
	}
	
	//METHODS
	public void getStudentNumber(int sNumber) {
		System.out.println("Your student number is:" + sNumber);
	}
	
	public void welcomeToStudent(String name) {
		System.out.println("Welcome, " + name);
	}
	
	public void learn(int amountStudied) {
		if(amountStudied > 0) {
			this.amountLearnt += amountStudied;
		}
	}
	
	private boolean helper(int amountStudied) {
		if(amountStudied > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public char getGrade() {
		return this.grade;
	}
	
	public int getStudentNumber() {
		return this.sNumber;
	}
	
	public int getAmountLearnt() {
		return this.amountLearnt;
	}


	
	public void setStudentNumber(int newSNumber) {
		this.sNumber = newSNumber;
	}
	
}